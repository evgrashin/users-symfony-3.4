+function ($) {

    $(document).ready(function() {
        $('.js-header-search-toggle').on('click', function() {
            $('.search-bar').slideToggle();
        });
    });

}(jQuery);

$(".select-option").on('change', function() {
    sortTable(this);
});

// Set GET parameters for sort.
function sortTable(e) {
    let sort_by = $("#select-sort-field").val();
    let dir = $("#select-sort-dir").val();

    const params = new URLSearchParams(window.location.search);

    params.set('sort_by', sort_by);
    params.set('dir', dir);

    window.history.replaceState({}, '', `${location.pathname}?${params}`);
    location.reload();
};