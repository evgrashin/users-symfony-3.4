<?php
/**
 * Created by PhpStorm.
 * User: evgrashin
 * Date: 11.01.19
 * Time: 14:15
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MainController
 * @package AppBundle\Controller
 */
class MainController extends Controller
{
   /**
     * @Route("/", name="app_homepage")
     * @return Response
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $html = $this->render('default/index.html.twig', [
            'user' => $user,
        ]);
        return new Response($html);
    }

    /**
     * @Route("/admin", name="admin_page")
     * @return Response
     */
    public function adminAction()
    {
        return new Response('<!doctype html><html lang="en"><body>Admin page!</body></html>');
    }

    /**
     * @Route("/secret", name="secret_page")
     * @Security("has_role('ROLE_ADMIN')")
     * @return Response
     */
    public function secretAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access');

        $user = $this->getUser();

        return new Response('Secret page. Hi, '.$user->getLogin());
    }
}