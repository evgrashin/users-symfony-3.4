<?php
/**
 * Created by PhpStorm.
 * User: evgrashin
 * Date: 17.01.19
 * Time: 14:39
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixtures
 * @package AppBundle\DataFixtures
 */
class UserFixtures extends Fixture
{
    private $encoder;

    /**
     * AppFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // create admin account
        $user = new User();
        $user->setLogin('admin');
        $user->setEmail('admin@mail.com');
        $user->setIsAdmin(true);
        $user->setGender('male');

        $password = $this->encoder->encodePassword($user, '123qwe');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();

        // create user account
        $user = new User();
        $user->setLogin('user');
        $user->setEmail('user@mail.com');
        $user->setGender('female');

        $password = $this->encoder->encodePassword($user, '123qwe');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();

        // create 10 fake accounts
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setLogin('login'.$i);
            $user->setEmail('email'.$i.'@mail.com');

            $password = $this->encoder->encodePassword($user, $user->getLogin().'123');
            $user->setPassword($password);

            $manager->persist($user);
            $manager->flush();
        }
    }
}
